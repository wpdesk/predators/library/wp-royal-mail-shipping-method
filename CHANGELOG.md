## [1.1.0] - 2022-08-16
### Added
- en_GB and pl_PL translations

## [1.0.5] - 2022-07-05
### Fixed
- texts

## [1.0.3] - 2022-06-28
### Fixed
- domestic services

## [1.0.0] - 2022-06-26
### Added
- initial version
